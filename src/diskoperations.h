#ifndef __DISKOPERATIONS_H
#define __DISKOPERATIONS_H

#define SOURCE "./Pictures/"
#define FILEMARKER 8


int loadFromFile(PICTURES***);
int saveToFile(PICTURES**);
void listFiles();
#endif //__DISKOPERATIONS_H
