#include "main.h"
#include "menu.h"
#include "diskoperations.h"
#include "editfile.h"

int rotatePicture(PICTURES** rotateACTIVE)
{
    if(*rotateACTIVE == NULL)
        {
            printf("Error:\n");
            printf("%s", RED_COLOR);
            printf("No image to rotate, choose active picture first. \n");
            printf("%s", NORMAL_COLOR);
            system_pause();
            return 0;
        }
    else
        {
            printf("Rotating the image 90 degrees, clockwise \n\n");

            int Columns = (*rotateACTIVE)->Columns;
            int Rows = (*rotateACTIVE)->Rows;

            /* Dimensions of matrix changes */
            int RotatedRows = Columns;
            int RotatedCols = Rows;
            int rowSrc = 0, colSrc = 0; //Source matrix
            int rowDst = 0, colDst = 0; //Destination matrix

            /* Allocating memory for new size */
            int** Rotated = NULL;
            Rotated = allocateMatrix(RotatedRows, RotatedCols);
                if(Rotated == NULL)
                    {
                        printf("Picture rotation fail, please try again \n");
                        system_pause();
                        return 0;
                    }

            /* Copying values to the new shaped matrix */
            for(colSrc = 0 ; colSrc < Columns; colSrc++)
                {
                    for(rowSrc = Rows-1 ; rowSrc >= 0 ; rowSrc--)
                        {
                            Rotated[rowDst][colDst] = (*rotateACTIVE)->Table[rowSrc][colSrc];
                            colDst++;
                            if(colDst == RotatedCols)
                                {
                                    colDst = 0;
                                    rowDst++;
                                }
                        }
                }

            /* Releasing old matrix and replacing with Rotated - new shaped matrix */
            deallocateMatrix(&rotateACTIVE);

            (*rotateACTIVE)->Table = Rotated;
            (*rotateACTIVE)->Rows = RotatedRows;
            (*rotateACTIVE)->Columns = RotatedCols;
        }
    printf("Rotation succesfull, returning to menu \n\n");
    system("sleep 2");
    return 0;
}

    /* Function returns pointer (adress) of allocated memory for int** variable */
int** allocateMatrix(int Rows, int Cols)
{

                /* Allocation for **Table (rows) */
                int** Table = NULL;
                Table = (int**)malloc(Rows*sizeof(int*));
                    if (Table != NULL)
                        {
                            for(int i=0 ; i < Rows ; i++)
                                {
                                    /* Allocation for *Table (columns) */
                                    Table[i] = (int*)malloc(Cols*sizeof(int));
                                    if(Table[i] == NULL)
                                        {
                                            printf("*Allocation failed \n");
                                            free(Table);
                                            return NULL;
                                        }
                                }
                        }
                    else
                        {
                            printf("**Allocation failed \n");
                            return NULL;
                        }
    return Table;
}

void printPicture(PICTURES** printPicACTIVE)
{
    if((*printPicACTIVE) == NULL)
        {
            printf("Error:\n");
            printf("%s", RED_COLOR);
            printf("No image to do display, choose active picture first. \n");
            printf("%s", NORMAL_COLOR);
            system_pause();
        }
    else
        {
            printf("Image: %s \n", (*printPicACTIVE)->fileName);
            printf("%s", GREEN_COLOR);

            for(int i = 0; i < (*printPicACTIVE)->Rows; i++)
                {
                    for(int j = 0; j < (*printPicACTIVE)->Columns; j++)
                        {
                            printf("%d ", (*printPicACTIVE)->Table[i][j]);
                        }
                    printf("\n");
                }
            printf("%s", NORMAL_COLOR);
            system_pause();
        }

}
