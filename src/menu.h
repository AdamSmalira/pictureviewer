#ifndef __MENU_H
#define __MENU_H

#define NORMAL_COLOR  "\x1B[0m"
#define RED_COLOR "\x1B[31m"
#define GREEN_COLOR "\x1B[32m"
#define YELLOW_COLOR "\x1B[33m"

void displayMenu(const int*, PICTURES**, PICTURES**);
void makeActive(const int*, PICTURES**, PICTURES**);
int createStruct(int*, PICTURES**);
int enlargeStruct(int*, PICTURES**, PICTURES**);
void getMemory(const int*, PICTURES***);
void removeStruct(int*, PICTURES**, PICTURES**);
void deallocateMatrix(PICTURES***);
void exitProgram(const int*, PICTURES**);
void system_pause();



#endif //__MENU_H
