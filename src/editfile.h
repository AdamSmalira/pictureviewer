#ifndef __EDITFILE_H
#define __EDITFILE_H

int rotatePicture(PICTURES**);
int** allocateMatrix(int, int);
void printPicture(PICTURES**);

#endif //__EDITFILE_H
