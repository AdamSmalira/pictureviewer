#ifndef __MAIN_H
#define __MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>

typedef struct PICTURES {
    char fileName[20];
    char fullPath[128];
    char Stnd[3];
    int Rows;
    int Columns;
    int Scale;
    int** Table;
}PICTURES;

#endif
