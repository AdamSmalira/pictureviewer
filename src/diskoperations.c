#include "main.h"
#include "menu.h"
#include "diskoperations.h"
#include "editfile.h"

static void comments(char* line, FILE** inc)
{
    char buffer[128];
    do
    {
        fgets(buffer, 127, *inc);
    }
    while (buffer[0] == '#');
    strcpy(line, buffer);
}

int loadFromFile(PICTURES*** loadFrTABLE)
{
    /* work on copy */
    PICTURES* loadTEMP;
    loadTEMP = **loadFrTABLE;

    /* asking user for input */
    system("clear");
    printf("%sImage files found in %s directory\n", NORMAL_COLOR, SOURCE);
    listFiles();
    printf("%sType full name of a picture and press enter: (for ex. test.pgm)\n", NORMAL_COLOR);
    scanf("%20s", loadTEMP->fileName);

    /* glueing full pathway */
    strcpy( loadTEMP->fullPath, SOURCE);
    strcat( loadTEMP->fullPath, loadTEMP->fileName);

    /* open access to the file */
    FILE *PickFile;
    PickFile = fopen(loadTEMP->fullPath, "rt");

    if(PickFile == NULL)
    {
        printf("%s \n", strerror(errno));
        /* File open fail, function menu.c/createStruct(), catches this for StructMemory deallocation */
        return 2;
    }
    else
    {
        char lineFeed[128];
        fgets(lineFeed, 127, PickFile);

        /* File version check */
        if (strcmp( lineFeed, "P2\n" ) != 0)
        {
            printf("File format is not supported: %s", lineFeed);
            fclose(PickFile);
            system_pause();
            return 0;
        }
        else
        {
            /* writing Version to variable [1st line from file] */
            strcpy(loadTEMP->Stnd, lineFeed);

            /* writing Cols and Rows to the variables [2nd line from file] */
            comments(lineFeed, &PickFile);
            sscanf(lineFeed, "%d %d", &loadTEMP->Columns, &loadTEMP->Rows);

            /* writing Scale to the variable [3rd line from file] */
            comments(lineFeed, &PickFile);
            sscanf(lineFeed, "%d", &loadTEMP->Scale);
        }

        /* creating Table[Rows][Columns] */
        loadTEMP->Table = allocateMatrix( loadTEMP->Rows, loadTEMP->Columns );


        if(loadTEMP->Table == NULL)
        {

            printf("Multi dim. array  allocation fail");
            fclose(PickFile);

            /* int** Table alloc fail, function menu.c/createStruct(), catches this for StructMemory DEallocation */
            /* int** Table alloc fail, function menu.c/enlargeStruct(), catches this for StructMemory REallocation */
            return 2;
        }
        else
        {
            for(int i = 0; i < loadTEMP->Rows; i++ )
            {
                for(int j = 0; j < loadTEMP->Columns; j++)
                {
                    fscanf(PickFile, "%d", &loadTEMP->Table[i][j]);
                }
            }
        }

    }
    fclose(PickFile);
    return 1;
}

int saveToFile(PICTURES** saveACTIVE)
{
    if(*saveACTIVE == NULL)
        {
            printf("No image to save, choose active picture first. \n");
            system_pause();
            return 0;
        }
    else
        {
        printf("Type new name of file to save: \n");
        printf("Warning: Typing name of existing file will override the existing file. \n\n");
        scanf("%20s",(*saveACTIVE)->fileName);


        strcpy((*saveACTIVE)->fullPath, SOURCE);
        strcat((*saveACTIVE)->fullPath, (*saveACTIVE)->fileName);


        /* saveTEMP = saveACTIVE */
        PICTURES* saveTEMP;
        saveTEMP = (PICTURES*)malloc(sizeof (PICTURES));
        if(saveTEMP == NULL)
        {
            printf("Error, please try again");
            system_pause();
            return 0;
        }
        memcpy(saveTEMP, *saveACTIVE, sizeof(PICTURES));

        /* opening file in Write Text mode */
        FILE *PickFile;
        PickFile = fopen(saveTEMP->fullPath, "wt");

        if(PickFile == NULL)
        {
            printf("%s\n", strerror(errno));
            free(saveTEMP);
            system_pause();
            return 0;
        }
        else
        {
            /* saving picture parameters */
            fputs(saveTEMP->Stnd, PickFile);
            fprintf(PickFile, "#File has been modified by PGM-Editor and saved as a: %s \n", saveTEMP->fileName);
            fprintf(PickFile, "%d %d\n",   saveTEMP->Columns, saveTEMP->Rows);
            fprintf(PickFile, "%d\n",      saveTEMP->Scale);

            /* saving matrix */
            for(int i = 0; i < saveTEMP->Rows ; i++)
            {
                for(int j = 0; j < saveTEMP->Columns; j++)
                {
                    fprintf(PickFile, "%d ",   saveTEMP->Table[i][j]);
                }
                fputc('\n', PickFile);
            }
        }


        printf("File has been saved in: %s \n\n", saveTEMP->fullPath);
        free(saveTEMP);
        system_pause();
        return 0;
        }
}

void listFiles()
{
    DIR* drive;
    struct dirent* dir;
    drive = opendir(SOURCE);
    if(drive != NULL)
    {
        while( (dir = readdir(drive)) != NULL )
        {
            if(dir->d_type == FILEMARKER)
            {
                printf("%s%s\n", GREEN_COLOR, dir->d_name);
            }
        }
        closedir(drive);
    }
}