SRC = src

all: compileapp run

compileapp:
	cd $(SRC) && $(MAKE)

clean:
	cd $(SRC) && $(MAKE) clean
	rm *.out

run:
	./PGM-Editor.out
