PGM-Editor (Portable Graymap Editor)

1. Overwiev
2. User Manual
3. Compilation and file execution
4. Technical information
5. Credits


1. The PGM-Editor is designed to load .pgm pictures, edit, and save changes into new file.

2. Program shows main menu and performs following actions:
[1] Add picture to database - user will be asked to type filename to load, user can add as many pictures as he wants.
[2] Remove picture from database - user will be asked to choose picture from the list, chosen picture will be removed from database.
[3] Choose picture to edit - user will be asked to select picture from database to perform edit options.

[4] Print picture to the screen - will display the grayscale matrix on the screen of selected picture.
[5] Rotate 90 deg - will rotate matrix (picture) clockwise of selected picture.
[6] Save active picture as a new file - will save selected picture with new name typed by user.

[x] for exit - will terminate the program. 

Images .pgm are being read and saved in folder .\Pictures\

3. Program has been adapted to Linux environment from previous Windows version.
Creation of its origin is documented under this link: git clone https://bitbucket.org/SvenLedo/projektadam/src/master/

-compilation proceeds through multi makefiles. In terminal, typing 'make' command from program directory, starts compilation process with gcc compiler.
Main makefile runs secondary makefile inside /src/ directory where source files are being stored.
First, source files are being compiled to ".o" - objects outputs.
Second, object files are being linked, creating executable file "PGM-Editor.out" in main directory.

-using command 'make clean' removes .o and .out files leaving source files.

-initiation goes through 'make run' command.

4. Program has been written using MS Visual Studio Code.
Main task was to use dynamically memory on demand with first load of a file -malloc().
Loaded data (first image) is being stored in structure, what holds detailed fields describing, size and other details of the picture.
After identification size of picture (amount of columns and rows) program dynamically allocates memory for multidimensional matrix to hold grayscale value of each pixel.

If first file has been loaded successfully, program fills structure with data from the file and finally picture name appears in menu (program database).
If second and later files has been added, allocated structure is being resized with every successfully file load -realloc().
That redefines now structure to an array of structures.
User can add as many pictures as he wants (array of structures will be collaterally resized) and pick one from the database.
Chosen picture will appear as a active picture to edit. This is accessed by pointer what points on selected index of an struct-array.
If user forgets to select active picture and will try to run edit option, program will warn user to choose picture first (edit options are performed on pointer, not on array of structures).
Same safety mechanism is used with save to file, or other edit options.
If user removes a file from database, first, program releases allocated multidimensional matrix of being deleted structure.
Second, sorts array of structures (replaces empty gap), and at end -realloc() downgrades size of structure by one.
If user removes a last structure, program releases resources using function free().

Program is secured for memory leaks, and returns error codes during allocation, load or save file. 
It will follow error algorithm to free allocated memory for matrix, and downgrades array of structures. 
While safety mechanism succeeds, warning is being displayed, and user can continue using the program (program does not exit).
PGM-Editor omits coments followed by "#" in .pgm file.

5. The PGM-Editor has been written by Adam Smalira, adam.smalira@mail.com 
This project shows technologies representing coding skills and knowledge of the developer in area of C programing.